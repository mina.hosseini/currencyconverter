package com.example.currencyconverter.helper

/**
 * All URL listed in this class
 */

class EndPoints {

    companion object {

        //Base URL
        const val BASE_URL = "http://api.exchangeratesapi.io/v1/"

        //API KEY
        const val API_KEY = "163792cdc0ad1f2fbe786b91cb34d4f3"

        //LATEST URL
        const val  LATEST_URL = "latest"

    }

}