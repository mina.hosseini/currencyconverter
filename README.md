# CurrencyConverter
## This App Uses

Kotlin

MVVM (Model View ViewModel Pattern)

Hilt (For Dependency Injection)

Retrofit (For making API calls)

Flow

LiveData

Coroutines (For simplifying Asynchronous operations)

View Binding (For interacting with views)

Some other libraries


